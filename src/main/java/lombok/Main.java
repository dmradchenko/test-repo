package lombok;

import java.util.*;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {

        Map<Integer, List<Book>>  books = new HashMap<>();

        Book book = Book.builder()
                .autor("JR")
                .name("HP")
                .size(730)
                .pdfFormat(true)
                .build();
        books.put(1, Arrays.asList(book, new Book()));
        book = Book.builder()
                .autor("JL")
                .size(348)
                .pdfFormat(false)
                .build();
        books.put(3, Arrays.asList(book, new Book()));
        List<Book> booksList = books.entrySet().stream()
                .flatMap(x -> x.getValue().stream())
                .collect(Collectors.toList());
        booksList.stream()
                .forEach(System.out::println);
        System.out.println(booksList.size());

    }

}
