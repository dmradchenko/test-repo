import javafx.util.Pair;

import java.sql.Struct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Airports {


    /**
     * На вході: (стрінг з парами аеропортів, аеропорт from, аеропорт to).
     * На виході: true - якщо такий рейс існує, false - якщо такого рейсу не існує.
     */


    public static Map<String, List<String>> getPairsOfAiroports(String airportPairs){
        return Stream.of(airportPairs.split("\n"))
                .distinct()
                .collect(Collectors.groupingBy(x -> x.substring(0,3),
                        Collectors.mapping(x -> x.substring(3,6), Collectors.toList())));
    }

    public static boolean isExists(String airportPairs, String from, String to){
        Map<String, List<String>> airoportsMap = getPairsOfAiroports(airportPairs);
        return airoportsMap.containsKey(from) && airoportsMap.get(from).contains(to);
    }


}
