package spring.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@ToString
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class GlossEntry {

    @JsonProperty("ID")
    String ID;
    @JsonProperty("SortAs")
    String sortAs;
    @JsonProperty("GlossTerm")
    String glossTerm;
    @JsonProperty("Acronym")
    String acronym;
    @JsonProperty("Abbrev")
    String abbrev;
    @JsonProperty("GlossDef")
    GlossDef glossDef;
    @JsonProperty("GlossSee")
    String glossSee;


}
