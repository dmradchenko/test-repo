package spring.models;

import lombok.*;

@Data
@ToString
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class JsonClass {

    Glossary glossary;


}
