package spring.models;


import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@NoArgsConstructor
@ToString
public class Status {

    String status;

    public Status(String status) {
        this.status = status;
    }
}
