import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AirportsTest {

    Airports airports;

    @Before
    public void setUp() throws Exception {
        airports = new Airports();
    }

    @Test
    public void getPairsOfAiroports() {
        String airportPairs = "RFRGFJ\nIYHDFX\nRHASDJ\nRNJCHD\nFGOITL\nYDFDKJ\nCHDFDO\nRCHDCH\nRNJDHD\nRNJERF";
        Map<String, List<String>> exp = new HashMap<>();
        exp.put("RFR", new ArrayList<>(Arrays.asList("GFJ")));
        exp.put("IYH", new ArrayList<>(Arrays.asList("DFX")));
        exp.put("RHA", new ArrayList<>(Arrays.asList("SDJ")));
        exp.put("RNJ", new ArrayList<>(Arrays.asList( "CHD", "DHD", "ERF")));
        exp.put("FGO", new ArrayList<>(Arrays.asList("ITL")));
        exp.put("YDF", new ArrayList<>(Arrays.asList("DKJ")));
        exp.put("CHD", new ArrayList<>(Arrays.asList("FDO")));
        exp.put("RCH", new ArrayList<>(Arrays.asList("DCH")));
        assertEquals(exp, airports.getPairsOfAiroports(airportPairs));
    }

    @Test
    public void isExists() {
        String airportPairs = "RFRGFJ\nIYHDFX\nRHASDJ\nRNJCHD\nFGOITL\nYDFDKJ\nCHDFDO\nRCHDCH\nRNJDHD";
        Map<String, List<String>> exp = new HashMap<>();
        exp.put("RFR", new ArrayList<>(Arrays.asList("GFJ")));
        exp.put("IYH", new ArrayList<>(Arrays.asList("DFX")));
        exp.put("RHA", new ArrayList<>(Arrays.asList("SDJ")));
        exp.put("RNJ", new ArrayList<>(Arrays.asList( "CHD", "DHD", "ERF")));
        exp.put("FGO", new ArrayList<>(Arrays.asList("ITL")));
        exp.put("YDF", new ArrayList<>(Arrays.asList("DKJ")));
        exp.put("CHD", new ArrayList<>(Arrays.asList("FDO")));
        exp.put("RCH", new ArrayList<>(Arrays.asList("DCH")));
        assertTrue(airports.isExists(airportPairs, "FGO", "ITL"));
        assertFalse(airports.isExists(airportPairs, "UYO", "ITL"));
    }
}