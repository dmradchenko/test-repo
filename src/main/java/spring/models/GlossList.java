package spring.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@ToString
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class GlossList {

    @JsonProperty("GlossEntry")
    GlossEntry glossEntry;

}
