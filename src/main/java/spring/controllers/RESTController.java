package spring.controllers;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mysql.fabric.Response;
import org.springframework.web.servlet.resource.HttpResource;
import spring.models.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@RestController
public class RESTController {

    /**
     * task 1
     */
    @RequestMapping(value = "/number", method = RequestMethod.GET)
    public int number(){
        Random random = new Random();
        return random.nextInt();
    }
    /**
     * task 2
     */
    @RequestMapping(value = "/sum/{first}", method = RequestMethod.GET)
    public int sum(@PathVariable int first, @RequestParam("second") int second){
        return first + second;
    }
    @RequestMapping(value = "/headers", method = RequestMethod.POST)
    public String headers(HttpServletRequest request){
        Enumeration headersName = request.getHeaderNames();
        Map<String, String> mapHeaders = new HashMap<>();
        String head = "Headers:\n";
        while (headersName.hasMoreElements()){
            String name = (String) headersName.nextElement();
            mapHeaders.put(name, request.getHeader(name));
        }
        return head + mapHeaders.toString();
    }

    /**
     * task 3
     */
    @RequestMapping(value = "/cookies", method = RequestMethod.PUT)
    public String cookie(HttpServletRequest request, HttpServletResponse response){
        Cookie[] cookies = request.getCookies();
        String cook = "Cookies:\n";
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                cook += cookie.getName() + " " + cookie.getValue() + "\n";
            }
        }
        return cook;
    }

    /**
     * task 4
     */
    @RequestMapping(value = "/cookies/{name}/{value}", method = RequestMethod.PUT)
    public void newCookie(@PathVariable(value = "name") String name, @PathVariable(value = "value") String value, HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.addCookie(new Cookie(name, value));
        response.sendRedirect("/cookies");
    }

    /**
     * task 6
     */
    @RequestMapping(value = "/redirect", method = RequestMethod.GET)
    public void redirect(HttpServletResponse response) throws IOException {
        response.sendRedirect("/redirected");

    }

    @RequestMapping(value = "/redirected", method = RequestMethod.GET)
    public String redirected(){
        return "Redirected";
    }
    /**
     * task 5,7
     */

    @RequestMapping(value = "/auth", method = RequestMethod.POST)
    public void auth(@RequestBody User user, HttpServletResponse resource){
           if(User.getStatus(user).getStatus().equals("success")){
            resource.setStatus(200);
        }else {
               resource.setStatus(403);
        }
    }

    @RequestMapping(value = "/status", method = RequestMethod.GET)
    public Status getStatus(@RequestBody User user){
        return User.getStatus(user);
    }

    @RequestMapping(value = "/json/{login}/{pass}", method = RequestMethod.GET)
    public User writeToJSON(@PathVariable(name = "login") String login, @PathVariable(name = "pass") String pass) throws JsonProcessingException, JsonProcessingException {
        User user = User.builder()
                .login(login)
                .pass(pass)
                .status(new Status("success"))
                .build();

        return user;
    }

    @RequestMapping(value = "/toJson", method = RequestMethod.GET)
    public String createJson() throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        JsonClass jsonClass = new JsonClass(
                new Glossary("example glossary", new GlossDiv("S", new GlossList(new GlossEntry("SGML", "SGML", "Standard Generalized Markup Language", "SGML", "ISO 8879:1986", new GlossDef("A meta-markup language, used to create markup languages such as DocBook.", new ArrayList<>(Arrays.asList("GML", "XML"))), "markup")))));
        return mapper.writeValueAsString(jsonClass);
    }

    @RequestMapping(value = "/size", method = RequestMethod.POST)
    public int arraySize(@RequestBody JsonClass jsonClass){
        return jsonClass.getGlossary().getGlossDiv().getGlossList().getGlossEntry().getGlossDef().getGlossSeeAlso().size();
    }



}
