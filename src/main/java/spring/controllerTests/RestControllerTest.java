package spring.controllerTests;

import spring.models.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class RestControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void numberTest() {
        Integer first = this.restTemplate.getForObject("/number", Integer.class);
        Integer second = this.restTemplate.getForObject("/number", Integer.class);
        assertTrue(!first.equals(second));
    }

    @Test
    public void sumTest() {
        int sum = this.restTemplate.getForObject("/sum/45?second=55", Integer.class);
        assertEquals(100, sum);
        sum = this.restTemplate.getForObject("/sum/-45?second=55", Integer.class);
        assertEquals(10, sum);
    }

    @Test
    public void redirectTest() {
         String body = this.restTemplate.getForObject("/redirect", String.class);
        assertEquals("Redirected", body);
    }

    @Test
    public void redirectedTest() {
        String body = this.restTemplate.getForObject("/redirected", String.class);
        assertEquals("Redirected", body);;
    }


    @Test
    public void sizeTest() {
        List<String> list = new ArrayList<>(Arrays.asList("GML", "XML","GML", "XML","GML", "XML","GML"));
        JsonClass jsonClass = new JsonClass(
                new Glossary("example glossary", new GlossDiv("S", new GlossList(new GlossEntry("SGML", "SGML", "Standard Generalized Markup Language", "SGML", "ISO 8879:1986", new GlossDef("A meta-markup language, used to create markup languages such as DocBook.", list), "markup")))));

        ResponseEntity<Integer> size = this.restTemplate.postForEntity("/size", jsonClass, Integer.class);
        assertEquals(HttpStatus.OK, size.getStatusCode());
        assertEquals(7, size.getBody().intValue());
    }
}
