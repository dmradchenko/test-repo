package spring.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Data
@ToString
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class GlossDef {

    String para;
//    String para2;
    @JsonProperty("GlossSeeAlso")
    List<String> glossSeeAlso;

}
