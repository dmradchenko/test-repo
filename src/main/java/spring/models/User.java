package spring.models;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@EqualsAndHashCode(of = {"login", "pass"})
public class User {

    String login;
    String pass;

    Status status = new Status("success");

    public User(String login, String pass) {
        this.login = login;
        this.pass = pass;
    }

    public static Status getStatus(User user) {
        if(user.login.equals("login") && user.pass.equals("pass")){
            return new Status("success");
        }else {
            return new Status("not success");
        }
    }
}
