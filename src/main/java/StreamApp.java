import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamApp {

    public static String numToText(int num){
        switch (num){
            case 1: return "one";
            case 2: return "two";
            case 3: return "three";
            case 4: return "four";
            case 5: return "five";
            case 6: return "six";
            case 7: return "seven";
            case 8: return "eight";
            case 9: return "nine";
            case 10: return "ten";
            default:return "zero";

        }
    }

    /**
     * 1. дано: массив строк. найти i-тый элемент, иначе возвращаем строку “MISSING”
     */

    public  String getString(int i, String[] strings){
        return Arrays.stream(strings)
                .skip(i).findFirst().orElse("MISSING");
    }

    /**
     * 2. взять с i по j элемент из стрима (допускаем что в массиве больше j элементов
     */
    public  String[] subArray(String[] strings, int i, int j){
        return Arrays.stream(strings)
                .skip(i)
                .limit(j-i)
                .toArray(size -> new String[size]);
    }


    /**
     * 3. то же самое, что и 2, но если нет всех элементов между i и j вернуть пустой список
     *
     * */

    public List<String> subList(List<String> strings, Integer i, Integer j){
        return strings.stream()
                .skip(i)
                .limit(j-i)
                .collect(Collectors.collectingAndThen(
                        Collectors.toList(),
                        result -> {
                            if(result.size() < (j-i)){
                                return new ArrayList<>();
                            }
                            return result;
                        }
                ));

    }


    /**
     * 4. из стрима строк содержащих числа сделать стрим чисел, которые потом возвести в куб
     */

    public  List<Integer> stringToInteger(List<String> numbers){
        return numbers.stream()
                .map(x -> Integer.valueOf(x))
                .map(x -> x*x*x)
                .collect(Collectors.toList());
    }

    /**
     * 5. преобразовать стрим чисел в стрим текстового представления (отдельная функция,
     * 1 = one, 5 = five, etc) числа, и выбрать самое длинное слово (five, т.к. длиннее one)
     */
    public  String getLongest(List<Integer> list){
        return list.stream().map(x -> numToText(x)).
                max(Comparator.comparingInt(String::length))
                .orElseThrow(() -> new IllegalArgumentException(String.format("Null!")));
    }

    /**
     * 6. выбрать уникальные строки с использованием стримов (2 способа)
     */

    public  List<String> firstUniqueString(List<String> list){
        return list.stream()
                .distinct()
                .collect(Collectors.toList());
    }

    public List<String> secondUniqueString(List<String> list){
        return new ArrayList<>(list.stream()
                .collect(Collectors.toSet()));
    }

    /**
     * 7. из стрима чисел получить карту значений, где ключ - текстовое представление числа, а значение - число умноженное на 2
     */

    public Map<String, Integer> getMap(List<Integer> list){
        return list.stream()
                .collect(Collectors.toMap(StreamApp::numToText, x -> x * 2 ));
    }

    /**
     * 8. из карты пункта 7 сделать карту наоборот - поменять ключ и значения местами (пара способов)
     */

    public  Map<Integer, String> inverseMap(Map<String, Integer> map){
        return map.entrySet()
                .stream()
                .collect(Collectors.toMap(k -> k.getValue(), k -> k.getKey()));
    }


    /**
     * сделать метод, который принимает параметр i, которое задает самое
     * большое число в стриме. первый элемент стрима 2
     * вывести на экран пара значений - текущий элемент и “число“. “число” это
     * дробное число, полученное из текущего элемента поделенного на (текущий элемент - 1).
     */

    public void pair(int max){
        Map<Integer, Double> map = Stream.iterate(2, x -> x + 1)
                .limit(max - 2 + 1)
                .collect(Collectors.toMap(x -> x, x -> (double)max/x));
        map.entrySet().stream().forEach(System.out::println);
    }

}
