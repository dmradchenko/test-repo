package spring.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import spring.jdbcModels.Book;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@RestController
@Component
public class CRUDController {

    private static JdbcTemplate jdbcTemplateObj;

    @Autowired
    public CRUDController(@Qualifier(value = "connection") JdbcTemplate jdbcTemplateObj) {
        this.jdbcTemplateObj = jdbcTemplateObj;
    }

    @RequestMapping(value = "/insert/{name}/{author}/{isPresent}/{pages}/{year}", method = RequestMethod.POST)
    public void insert(@PathVariable(value = "name") String name, @PathVariable(value = "author") String author,
                        @PathVariable(value = "isPresent") boolean isPresent, @PathVariable(value = "pages") Integer pages,
                       @PathVariable(value = "year") Integer year){
        String sqlInsertQuery = "INSERT INTO book (name, author, isPresent, pages, year) VALUES (?, ?, ?, ?, ?)";

        jdbcTemplateObj.update(sqlInsertQuery, name, author, isPresent, pages, year);
    }

    @RequestMapping(value = "/upDate/{id}/{name}/{author}/{isPresent}/{pages}/{year}", method = RequestMethod.POST)
    public void upDate(@PathVariable(value = "id") Integer id, @PathVariable(value = "name") String name, @PathVariable(value = "author") String author,
                       @PathVariable(value = "isPresent") boolean isPresent, @PathVariable(value = "pages") Integer pages,
                       @PathVariable(value = "year") Integer year){
        String sqlInsertQuery = "UPDATE book set name=?, author=?, isPresent=?, pages=?, year=? where bookId = " + id;

        jdbcTemplateObj.update(sqlInsertQuery, name, author, isPresent, pages, year);
    }

    @RequestMapping(value = "/select", method = RequestMethod.GET)
    public String select(){
        String sqlInsertQuery = "SELECT bookId, name, author, isPresent, pages, year FROM book";

        List<Book> bookList = jdbcTemplateObj.query(sqlInsertQuery, new RowMapper<Book>() {
            @Override
            public Book mapRow(ResultSet resultSet, int i) throws SQLException {
                return Book.builder()
                        .bookId(resultSet.getInt(1))
                        .name(resultSet.getString(2))
                        .author(resultSet.getString(3))
                        .isPresent(resultSet.getBoolean(4))
                        .pages(resultSet.getInt(5))
                        .year(resultSet.getInt(6))
                        .build();
            }
        });

        String books = bookList.toString().replace("), ", "\n");

        return books;
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public void deleteById(@PathVariable(value = "id") Integer id){
        String sqlInsertQuery = "DELETE FROM book where bookId = ?";

        jdbcTemplateObj.update(sqlInsertQuery, id);
    }

}
