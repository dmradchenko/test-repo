import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

public class StreamAppTest {

    StreamApp streamApp;



    @Before
    public void befire(){
        streamApp = new StreamApp();
    }

    /**
     * 1. дано: массив строк. найти i-тый элемент, иначе возвращаем строку “MISSING”
     */

    @Test
    public void getString() {
        String[] array = {"1", "2", "3", "4", "5"};
        int i = 6;
        String s = streamApp.getString(i, array);
        assertEquals("MISSING", s);
        assertNotNull(s);
        i = 3;
        s = streamApp.getString(i, array);
        assertEquals(array[i], s);
        assertNotEquals("MISSING", s);
        assertNotNull(s);

    }

    /**
     * 2. взять с i по j элемент из стрима (допускаем что в массиве больше j элементов
     */

    @Test
    public void subArray() {
        String[] array = {"1", "2", "3", "4", "5", "7", "8", "9", "10"};
        int i = 1;
        int j = 3;
        String []exp = {"2", "3"};
        String []s = streamApp.subArray(array, i, j);
        assertArrayEquals(exp, s);
        assertNotNull(s);
        i = 3;
        j = 6;
        exp = new String[]{"4", "5", "7"};
        s = streamApp.subArray(array, i, j);
        assertArrayEquals(exp, s);
        assertNotEquals("MISSING", s);
    }

    /**
     * 3. то же самое, что и 2, но если нет всех элементов между i и j вернуть пустой список
     *
     * */

    @Test
    public void subList() {
        List<String> list = new ArrayList<>(Arrays.asList("1", "2", "3", "4", "5", "7", "8", "9", "10"));
        int i = 1;
        int j = 3;
        List<String> expList = new ArrayList<>(Arrays.asList("2", "3"));
        List<String> s = streamApp.subList(list, i, j);
        assertEquals(expList, s);
        assertNotNull(s);
        i = 3;
        j = 15;
        expList = new ArrayList<>(Arrays.asList("4", "5", "7"));
        s = streamApp.subList(list, i, j);
        assertEquals(new ArrayList<>(), s);
        assertNotEquals(expList, s);
    }

    /**
     * 4. из стрима строк содержащих числа сделать стрим чисел, которые потом возвести в куб
     */
    @Test
    public void stringToInteger() {
        List<String> list = new ArrayList<>(Arrays.asList("1", "2", "3", "4", "5"));
        List<Integer> expList = new ArrayList<>(Arrays.asList(1, 8, 27, 64, 125));
        List<Integer> s = streamApp.stringToInteger(list);
        assertEquals(expList, s);
        assertNotNull(s);
        expList = new ArrayList<>(Arrays.asList(1, 8, 27, 34, 215));
        s = streamApp.stringToInteger(list);
        assertNotEquals(expList, s);
    }

    /**
     * 5. преобразовать стрим чисел в стрим текстового представления (отдельная функция,
     * 1 = one, 5 = five, etc) числа, и выбрать самое длинное слово (five, т.к. длиннее one)
     */

    @Test
    public void getLongest() {
        List<Integer> list = new ArrayList<>(Arrays.asList(1, 8, 6, 2, 7));
        String s = streamApp.getLongest(list);
        assertEquals("eight", s);
        assertNotNull(s);
        list = new ArrayList<>(Arrays.asList(1, 2, 10));
        s = streamApp.getLongest(list);
        assertEquals("one", s);
        assertNotEquals("two", s);
    }

    /**
     * 6. выбрать уникальные строки с использованием стримов (2 способа)
     */
    @Test
    public void firstUniqueString() {
        List<String> list = new ArrayList<>(Arrays.asList("1","3", "4", "5", "7", "2", "3", "4", "5"));
        List<String> expList = new ArrayList<>(Arrays.asList("1", "3", "4", "5", "7", "2"));
        List<String> s = streamApp.firstUniqueString(list);
        assertEquals(expList, s);
        assertNotNull(s);
        expList = new ArrayList<>(Arrays.asList("1","3", "4", "5", "7", "2", "3"));
        s = streamApp.firstUniqueString(list);
        assertNotEquals(expList, s);
    }


    /**
     * 7. из стрима чисел получить карту значений, где ключ - текстовое представление числа, а значение - число умноженное на 2
     */
    @Test
    public void getMap() {
        List<Integer> list = new ArrayList<>(Arrays.asList(1, 3, 4, 5, 7));
        Map<String, Integer> expMap = new HashMap<>();
        expMap.put("one", 2);
        expMap.put("three", 6);
        expMap.put("four", 8);
        expMap.put("five", 10);
        expMap.put("seven", 14);
        Map<String, Integer> map = streamApp.getMap(list);
        assertEquals(expMap, map);
    }

    /**
     * 8. из карты пункта 7 сделать карту наоборот - поменять ключ и значения местами (пара способов)
     */


    @Test
    public void inverseMap() {
        Map<String, Integer> map = new HashMap<>();
        map.put("one", 2);
        map.put("three", 6);
        map.put("four", 8);
        map.put("five", 10);
        map.put("seven", 14);
        Map<Integer, String> expMap = new HashMap<>();
        expMap.put(2, "one");
        expMap.put(6, "three");
        expMap.put(8, "four");
        expMap.put(10, "five");
        expMap.put(14, "seven");
        Map<Integer, String> invMap = streamApp.inverseMap(map);
        assertEquals(expMap, invMap);
        expMap.put(14, "six");
        assertNotEquals(expMap, invMap);
    }

    /**
     * сделать метод, который принимает параметр i, которое задает самое
     * большое число в стриме. первый элемент стрима 2
     * вывести на экран пара значений - текущий элемент и “число“. “число” это
     * дробное число, полученное из текущего элемента поделенного на (текущий элемент - 1).
     */

    @Test
    public void pair() {
        Map<Integer, Double> numbers = new HashMap<>();
        int max = 5;
        numbers.put(2, (double) (max/2));
        numbers.put(3, (double) (max/3));
        numbers.put(4, (double) (max/4));
        numbers.put(5, (double) (max/5));
        String exp = "2=" + (double)max/2 + "\n3=" + (double)max/3 + "\n4=" + (double)max/4 + "\n5=" + (double)max/5 +"\n";
        final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        streamApp.pair(max);
        assertEquals(exp, outContent.toString());
    }
}