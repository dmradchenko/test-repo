package reflection;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(of = {"firstField", "secondField"})
public class MyClass {

    private Integer firstField;
    public String secondField;
    public boolean thirdField;
}
