package lombok;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(of = {"author", "name", "size"})
public class Book {

    String autor;
    String name;
    Integer size;
    Boolean pdfFormat;
}
