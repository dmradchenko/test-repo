package reflection;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PeflectionApp {

    /**
     * 1) создаешь объект этого класса
     * и вычитываешь из него приватную переменную
     */

    public void getPrivateField(int param){
        Reflection reflection = new Reflection(param);
        int a = 0;
        try {
            Field field = reflection.getClass().getDeclaredField("a");
            field.setAccessible(true);
            a = field.getInt(reflection);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        System.out.println(a);
    }

    /**
     * 2) создать любой класс. Написать методы, поля.
     * С помощью рефлексии вывести список доступных полей и методов.
     */
    public void getFieldsAnaMethods(){
        MyClass myClass = new MyClass(15, "StringField", true);
        List<Field> fieldsList = new ArrayList<Field>(Arrays.asList(myClass.getClass().getDeclaredFields()));
        List<Method> methodsList = new ArrayList<Method>(Arrays.asList(myClass.getClass().getDeclaredMethods()));
        System.out.println("Fields:");
        fieldsList.stream().forEach(System.out::println);
        System.out.println("Methods:");
        methodsList.stream().forEach(System.out::println);
    }

    /**
     * 3) Создать класс без использования оператора new (с помощью рефлексии)
     */
    public void createInstansce(){
        try {
            Class inst = Class.forName("reflection.MyClass");
            Object myClass1 = inst.newInstance();
            System.out.println(myClass1.getClass());
            System.out.println(myClass1.toString());
            Constructor constructor = Class.forName("reflection.MyClass").getConstructor(Integer.class, String.class, boolean.class);
            Object myClass2 = constructor.newInstance(13, "SecondField", true);
            System.out.println(myClass2.getClass());
            System.out.println(myClass2.toString());
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

}
