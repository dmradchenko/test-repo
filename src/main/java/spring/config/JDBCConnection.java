package spring.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

import java.sql.SQLException;

@Configuration
@ComponentScan
public class JDBCConnection {

    static SimpleDriverDataSource dataSourceObj;

    @Autowired
    Environment environment;

    @Bean(name = "connection")
    public JdbcTemplate getDatabaseConnection(){
        dataSourceObj = new SimpleDriverDataSource();
        try {
            dataSourceObj.setDriver(new com.mysql.jdbc.Driver());
            dataSourceObj.setUrl(environment.getProperty("spring.datasource.url"));
            dataSourceObj.setUsername(environment.getProperty("spring.datasource.username"));
            dataSourceObj.setPassword(environment.getProperty("spring.datasource.password"));
        } catch(SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return new JdbcTemplate(dataSourceObj);
    }


}
