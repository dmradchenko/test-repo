package spring.jdbcModels;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Book {

    private Integer bookId;
    private String name;
    private String author;
    private boolean isPresent;
    private int pages;
    private int year;

}
