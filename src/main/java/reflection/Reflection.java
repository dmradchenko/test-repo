package reflection;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public class Reflection {

    private int a;

    public Reflection(int a) {
        this.a = a;
    }

    public Reflection() {

    }

    @ValuesToParams(a = 3, b = 2)
    public static void sum(int a, int b){
        System.out.println(a * b);
    }


    public static void main(String[] args) {
        try {
            Method method = Reflection.class.getDeclaredMethod("sum", int.class, int.class);
            ValuesToParams valuesToParams = method.getAnnotation(ValuesToParams.class);
            sum(valuesToParams.a(), valuesToParams.b());
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

}
