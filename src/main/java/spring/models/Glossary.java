package spring.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@ToString
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class Glossary {

    String title;

    @JsonProperty("GlossDiv")
    GlossDiv glossDiv;

}
